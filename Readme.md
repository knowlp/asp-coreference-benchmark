# Contents

* 2 Datasets (21 + 33 instances) for coreference adjudication (see JETAI paper).

* ASP Encodings of experiments submitted to LPNMR 2017.

# Citation

* Peter Sch�ller.
  Adjudication of Coreference Annotations via Answer Set Optimization.
  Journal of Experimental & Theoretical Artificial Intelligence, 2018.
  In press, [arXiv:1802.00033](http://arxiv.org/abs/1802.00033).

# Other References

* Peter Sch�ller.
  Adjudication of Coreference Annotations via Answer Set Optimization.
  In: Logic Programming and Nonmonotonic Reasoning (LPNMR), pages 343-357, 2017.

# Detailed Contents

* `encodings/core`

  Core encodings without objectives.

* `encodings/objectives`

  Objective functions, intended to be used together with encodings.

* `dataset1/` and `dataset2/`
  
  Datasets, given anonymized as set of ASP facts.

# License

  Automatic Coreference Adjudication based on Answer Set Programming
  Copyright (C) 2015-2017 Peter Sch�ller <schueller.p@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.